package com.example.flappybirdproject

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.animation.AccelerateInterpolator
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.FragmentManager
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*
import kotlin.math.abs
import kotlin.math.max
import kotlin.random.Random

class MainActivity : AppCompatActivity() {
    val maxpower=20 //кол-во "энергии", даваемое при прыжке птички
    var isStart=false //переменная, отвечающая за текущее состояние игры
    var power=maxpower //количество "энергии" птички в данный моммент
    var time=0
    var score=0
    var spaceY=10
    var spaceX=10
    var type = 0 //тип управления
    var y = 0f //для свайпа
    var booba = true //для гироскопа

    lateinit var sensorManager : SensorManager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        isStart=false
        context = this

        //Подготовка гироскопа
        sensorManager = getSystemService(SENSOR_SERVICE) as SensorManager
        val sensor = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE)
        if (sensor != null) {
            //Слушаем события
            sensorManager.registerListener(object : SensorEventListener {
                override fun onSensorChanged(p0: SensorEvent?) {
                    if (controller.isEnabled) {
                        if (type == 2) {
                            Log.d("errror", "${p0!!.values[2]}, $booba")
                            if (p0!!.values[2] > 0.5 && booba) {
                                booba = false
                                smt()
                            } else if (p0!!.values[2] <= 0.5) {
                                booba = true
                            }
                        }
                    }
                }

                override fun onAccuracyChanged(p0: Sensor?, p1: Int) {}
            }, sensor, SensorManager.SENSOR_DELAY_GAME)
        } else {
            Toast.makeText(this, "У вас нет гироскопа", Toast.LENGTH_SHORT).show()
        }

        //Создание цикла игры
        val handler = Handler()
        Thread {
            val runnable = object : Runnable {
                override fun run() {
                    if (isStart) { //Проверка на состояние игры
                        //Передвижения объектов
                        bird.animate()
                            .setDuration(0)
                            .rotation(-power.toFloat())
                            .translationYBy(-power.toFloat())
                            .start()
                        tube1.animate()
                            .setDuration(0)
                            .translationXBy(-5f)
                            .start()
                        tube2.animate()
                            .setDuration(0)
                            .translationXBy(-5f)
                            .start()
                        power = max(power - 1, -40)
                        val stime = time%290
                        if (stime == 0) { //Создание новой трубы
                            score++
                            scoreText.text = score.toString()
                            val newy = abs(Random(Date().time).nextInt()) %800
                            tube1.animate()
                                .setDuration(0)
                                .translationY(-newy.toFloat()-200)
                                .translationX(0f)
                                .start()
                            tube2.animate()
                                .setDuration(0)
                                .translationY(-newy.toFloat()+1800)
                                .translationX(0f)
                                .start()
                        }
                        //Просчитывание столкновений
                        if ((stime in (168+spaceX)..(268-spaceX) && (bird.y<=tube1.y+1630-spaceY
                                    || bird.y>=tube2.y-120+spaceY)) || bird.y>grass.y || bird.y+100<sky.y) {
                            endGame()
                        }
                        time++
                    }
                    handler.postDelayed(this, 20)
                }
            }
            handler.post(runnable)
        }.start()

        //Настройка контроллера для свайпов и кликов
        controller.setOnTouchListener { view, motionEvent ->
            if (motionEvent.action == MotionEvent.ACTION_DOWN && type==1) {
                y=motionEvent.y
            } else if (motionEvent.action == MotionEvent.ACTION_UP) {
                if (type==0) {
                    smt()
                } else if (type==1 && y-motionEvent.y>50) {
                    smt()
                }
            }
            return@setOnTouchListener true
        }

        updateLeaderBoard()
    }

    //Вспомогательная функция
    fun smt() {
        if (isStart) { //Если игра уже началась
            power = maxpower
        } else { //Если игру ещё не начали
            time = 0
            score = -1
            //Анимации
            scoreText.animate()
                .setDuration(200)
                .alpha(0.8f)
                .start()
            startText.animate()
                .setDuration(500)
                .alpha(0.2f)
                .translationY(-800f)
                .scaleX(0.7f)
                .scaleY(0.7f)
                .setInterpolator(AccelerateInterpolator())
                .start()
            context.leaderBoard.animate()
                .setDuration(500)
                .translationX(800f)
                .setInterpolator(AccelerateInterpolator())
                .start()
            controlls.animate()
                .setDuration(200)
                .translationX(300f)
                .setInterpolator(AccelerateInterpolator())
                .start()
            isStart = true
        }
    }

    //При завершении игры
    fun endGame() {
        isStart = false
        power=maxpower
        time=0
        controller.isEnabled = false

        //Анимация падения птички
        bird.animate()
            .setDuration(abs(grass.y-bird.y).toLong()/2)
            .translationY(810f)
            .rotation(0f)
            .setInterpolator(AccelerateInterpolator())
            .withEndAction {
                bird.animate()
                    .setDuration(20)
                    .scaleY(0.3f)
                    .withEndAction {
                        //Создание кастомного диалога
                        CustomDialog(score).show(supportFragmentManager, "Something")
                    }
                    .start()
            }
            .start()
    }

    companion object {
        lateinit var context: Activity

        //Начинаем игру заново
        fun startGame() {
            //Затемнение экрана
            context.back.animate()
                .setDuration(500)
                .alpha(0f)
                .withEndAction {
                    updateLeaderBoard()
                    //Возвращение объектов на их места
                    context.controlls.animate()
                        .setDuration(0)
                        .translationX(0f)
                        .start()
                    context.startText.animate()
                        .setDuration(0)
                        .alpha(1f)
                        .translationY(0f)
                        .scaleX(1f)
                        .scaleY(1f)
                        .start()
                    context.leaderBoard.animate()
                        .setDuration(0)
                        .translationX(0f)
                        .start()
                    context.scoreText.text = "0"
                    context.scoreText.animate()
                        .setDuration(0)
                        .alpha(0f)
                        .start()
                    context.bird.animate()
                        .setDuration(0)
                        .scaleY(1f)
                        .translationY(0f)
                        .rotation(0f)
                        .start()
                    context.tube1.animate()
                        .setDuration(0)
                        .translationX(0f)
                        .translationY(0f)
                        .start()
                    context.tube2.animate()
                        .setDuration(0)
                        .translationX(0f)
                        .translationY(0f)
                        .start()
                    //Убираем затемнение
                    context.back.animate()
                        .setDuration(500)
                        .alpha(1f)
                        .withEndAction {
                            context.controller.isEnabled=true
                        }
                        .start()
                }
                .start()
        }

        //Функция обновления топа
        fun updateLeaderBoard() {
            val sh = context.getSharedPreferences("myGame", 0)
            for (i in 1..5) {
                val k = sh.getInt("scoreNum$i", -1)
                val s = sh.getString("scoreStr$i", "Anonymous")
                if (k==-1) {
                    break
                }
                if (i == 1) { context.top1.text = s; context.topScore1.text = k.toString() }
                if (i == 2) { context.top2.text = s; context.topScore2.text = k.toString() }
                if (i == 3) { context.top3.text = s; context.topScore3.text = k.toString() }
                if (i == 4) { context.top4.text = s; context.topScore4.text = k.toString() }
                if (i == 5) { context.top5.text = s; context.topScore5.text = k.toString() }
            }
        }
    }

    //При переключении управления на тапы
    fun onTapClicked(v: View) {
        startText.text = "Tap to Play"
        type = 0
        imageView.setBackgroundResource(R.color.teal_700)
        imageView2.setBackgroundResource(R.color.teal_200)
        imageView3.setBackgroundResource(R.color.teal_200)
    }

    //При переключении управления на свайпы
    fun onSwipeClicked(v: View) {
        startText.text = "Swipe to Play"
        type = 1
        imageView.setBackgroundResource(R.color.teal_200)
        imageView2.setBackgroundResource(R.color.teal_700)
        imageView3.setBackgroundResource(R.color.teal_200)
    }

    //При переключении управления на гироскоп
    fun onGyroscopeClicked(v: View) {
        startText.text = "Move phone to Play"
        type = 2
        imageView.setBackgroundResource(R.color.teal_200)
        imageView2.setBackgroundResource(R.color.teal_200)
        imageView3.setBackgroundResource(R.color.teal_700)
    }
}