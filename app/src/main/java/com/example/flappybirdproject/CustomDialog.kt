package com.example.flappybirdproject

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.DialogFragment
import kotlinx.android.synthetic.main.item.view.*

class CustomDialog(var score: Int) : DialogFragment() {
    override fun onStart() {
        super.onStart()
        //Настройка окна
        val width = (resources.displayMetrics.widthPixels *0.9).toInt()
        val height = (resources.displayMetrics.heightPixels *0.4).toInt()
        dialog!!.window!!.setLayout(width, height)
        dialog!!.setCancelable(false)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val root = inflater.inflate(R.layout.item, container, false)
        root.myScoreText.text = score.toString()
        //Если пользователь записал хоть что-то
        root.name.addTextChangedListener {
            if (it!!.toString()=="") {
                root.textView2.text = "Сохранить анонимно"
            } else {
                root.textView2.text = "Сохранить"
            }
        }
        //При нажатии на сохранить
        root.textView2.setOnClickListener {
            var txt = root.name.text.toString()
            if (txt=="") {
                txt="Anonymous"
            }
            val sh = root.context.getSharedPreferences("myGame", 0)
            val ed = sh.edit()
            for (i in 1..5) { //Сохранение
                if (score > sh.getInt("scoreNum$i", -1)) {
                    for (j in i..5) {
                        val tmp = score
                        val tmpS = txt
                        score = sh.getInt("scoreNum$j", -1)
                        txt = sh.getString("scoreStr$j", "")!!
                        ed.putInt("scoreNum$j", tmp)
                        ed.putString("scoreStr$j", tmpS)
                    }
                    ed.commit()
                    break
                }
            }
            MainActivity.startGame()
            dialog!!.cancel()
        }
        return root
    }
}